<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\{Permission, Role};

class AssignController extends Controller
{
    public function create()
    {

        $roles = Role::get();
        $permissions = Permission::get();
        return view('permission.assigns.create', compact('roles', 'permissions'));
    }

    public function store(Request $request)
    {
        request()->validate([
            'role' => 'required',
            'permissions' => 'array|required'
        ]);

        $role = Role::find(request('role'));
        $role->givePermissionTo(request('permissions'));

        session()->flash('message', 'Successfully give permissions');
        return back();
    }

    public function edit(Role $role)
    {
        return view('permission.assigns.sync', [
            'role' => $role,
            'roles' => Role::get(),
            'permissions' => Permission::get()
        ]);
    }

    public function update(Role $role)
    {
        request()->validate([
            'role' => 'required',
            'permissions' => 'array|required'
        ]);

        $role->syncPermissions(request('permissions'));

        session()->flash('message', 'Successfully sync permissions');

        return redirect()->route('assign.create');
    }
}
