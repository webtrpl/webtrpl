<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Governance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GovernanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.governance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.governance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'period' => 'required',
            'image' => 'required',
        ]);

        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('upload');
        }

        Governance::create([
            'period' => request('period'),
            'body' => request('body'),
            'image' => $image,
            'slug' => Str::slug(request('period')),
        ]);

        session()->flash('message', 'Successfully Added');
        return redirect()->route('governances.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Governance $governance)
    {
        return view('backend.governance.edit', compact('governance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Governance $governance)
    {
        request()->validate([
            'period' => 'required',
        ]);

        $image = $governance->image ?? null;
        if ($request->hasFile('image')) {
            if ($governance->image) {
                Storage::delete($governance->image);
            }
            $image = $request->file('image')->store('public/upload');
        }

        $governance->update([
            'period' => request('period'),
            'body' => request('body'),
            'image' => $image,
            'slug' => Str::slug(request('period')),
        ]);

        session()->flash('message', 'Successfully Updated');
        return redirect()->route('governances.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Governance $governance)
    {
        $governance->delete();

        session()->flash('message', 'Successfully Delete');
        return redirect()->route('governances.index');
    }
}
