<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Vision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.vision.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.vision.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'welcome' => 'required',
            'vision' => 'required',
            'mission' => 'required',
            'goal' => 'required',
            'target' => 'required'
        ]);

        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/visions');
        }

        Vision::create([
            'welcome' => request('welcome'),
            'vision' => request('vision'),
            'mission' => request('mission'),
            'goal' => request('goal'),
            'target' => request('target'),
            'image' => $image
        ]);

        session()->flash('message', 'Successfully Added');

        return redirect()->route('visions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Vision $vision)
    {
        return view('backend.vision.edit', compact('vision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vision $vision)
    {
        request()->validate([
            'welcome' => 'required',
            'vision' => 'required',
            'mission' => 'required',
            'goal' => 'required',
            'target' => 'required'
        ]);

        $image = $vision->image ?? null;
        if ($request->hasFile('image')) {
            if ($vision->image) {
                Storage::delete($vision->image);
            }
            $image = $request->file('image')->store('public/visions');
        }

        $vision->update([
            'welcome' => request('welcome'),
            'vision' => request('vision'),
            'mission' => request('mission'),
            'goal' => request('goal'),
            'target' => request('target'),
            'image' => $image
        ]);

        session()->flash('message', 'Successfully Added');
        return view('backend.vision.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vision $vision)
    {
        $vision->delete();

        session()->flash('message', 'Successfully Delete');
        return view('backend.vision.index');
    }
}
