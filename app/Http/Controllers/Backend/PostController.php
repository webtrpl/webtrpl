<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.post.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.post.create', [
            'categories' => Category::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required|unique:posts,title',
            'body' => 'required',
            'image' => 'required',
            'categories' => 'required'
        ]);

        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/posts');
        }

        $category = Category::find($request->categories);

        $post = Post::create([
            'title' => request('title'),
            'body' => request('body'),
            'image' => $image,
            'slug' => Str::slug(request('title')),
            'user_id' => auth()->user()->id
        ]);

        $post->categories()->attach($category);

        session()->flash('message', 'Successfully Added');
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        return view('backend.post.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        request()->validate([
            'title' => 'required',
            'body' => 'required',
            'categories' => 'required'
        ]);

        $image = $post->image ?? null;
        if ($request->hasFile('image')) {
            if ($post->image) {
                Storage::delete($post->image);
            }
            $image = $request->file('image')->store('public/posts');
        }

        $category = Category::find($request->categories);

        $post->update([
            'title' => request('title'),
            'body' => request('body'),
            'image' => $image,
            'slug' => Str::slug(request('title')),
            'user_id' => auth()->user()->id
        ]);

        $post->categories()->sync($category);

        session()->flash('message', 'Successfully Updated');
        return view('backend.post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        session()->flash('message', 'Successfully Delete');
        return view('backend.post.index');
    }
}
