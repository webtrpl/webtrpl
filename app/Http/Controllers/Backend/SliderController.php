<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.slider.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'body' => 'required',
            'url_link' => 'required',
            'url_name' => 'required',
            'image' => 'required'
        ]);

        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/sliders');
        }

        Slider::create([
            'title' => request('title'),
            'body' => request('body'),
            'url_link' => request('url_link'),
            'url_name' => request('url_name'),
            'image' => $image
        ]);

        session()->flash('message', 'Successfully Added');

        return redirect()->route('sliders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {

        return view('backend.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        request()->validate([
            'title' => 'required',
            'body' => 'required',
            'url_link' => 'required',
            'url_name' => 'required',
        ]);

        $image = $slider->image ?? null;
        if ($request->hasFile('image')) {
            if ($slider->image) {
                Storage::delete($slider->image);
            }
            $image = $request->file('image')->store('public/sliders');
        }

        $slider->update([
            'title' => request('title'),
            'body' => request('body'),
            'url_link' => request('url_link'),
            'url_name' => request('url_name'),
            'image' => $image
        ]);

        session()->flash('message', 'Successfully Updated');

        return redirect()->route('sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        $slider->delete();
        session()->flash('message', 'Successfully Deleted');
        return view('backend.slider.index');
    }
}
