<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.course.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'code' => 'required',
            'name' => 'required',
            'credit' => 'required|numeric',
            'semester' => 'required|numeric',
            'description' => 'required'
        ]);

        Course::create([
            'code' => request('code'),
            'name' => request('name'),
            'credit' => request('credit'),
            'semester' => request('semester'),
            'description' => request('description')
        ]);

        session()->flash('message', 'Successfully Added');

        return redirect()->route('courses.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        return view('backend.course.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        request()->validate([
            'code' => 'required',
            'name' => 'required',
            'credit' => 'required|numeric',
            'semester' => 'required|numeric'
        ]);

        $course->update([
            'code' => request('code'),
            'name' => request('name'),
            'credit' => request('credit'),
            'semester' => request('semester'),
            'description' => request('description')
        ]);

        session()->flash('message', 'Successfully Updated');

        return redirect()->route('courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        session()->flash('message', 'Successfully Deleted');
        return view('backend.course.index');
    }
}
