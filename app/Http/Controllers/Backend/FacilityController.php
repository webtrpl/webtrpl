<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.facility.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.facility.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required'
        ]);

        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/upload');
        }

        Facility::create([
            'name' => request('name'),
            'description' => request('description'),
            'image' => $image
        ]);

        session()->flash('message', 'Facility Succesfully Added');

        return redirect()->route('facilities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Facility $facility)
    {
        return view('backend.facility.edit', compact('facility'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $image = $facility->image ?? null;
        if ($request->hasFile('image')) {
            if ($facility->image) {
                Storage::delete($facility->image);
            }
            $image = $request->file('image')->store('public/upload');
        }

        $facility->update([
            'name' => request('name'),
            'description' => request('description'),
            'image' => $image
        ]);

        session()->flash('message', 'Succesfully Updated');

        return redirect()->route('facilities.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Facility $facility)
    {
        $facility->delete();

        session()->flash('message', 'Succesfully Deleted');

        return redirect()->route('facilities.index');
    }
}
