<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Symbol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SymbolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.symbol.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('backend.symbol.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'images' => 'required',
        ]);

        $images = null;
        if ($request->hasFile('images')) {
            $images = $request->file('images')->store('public/upload');
        }

        Symbol::create([
            'images' => $images,
            'body' => request('body')
        ]);

        session()->flash('message', 'Successfully Added');
        return redirect()->route('symbols.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Symbol $symbol)
    {
        return view('backend.symbol.edit', compact('symbol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Symbol $symbol)
    {
        $images = $symbol->images;
        if ($request->hasFile('images')) {
            if ($symbol->images) {
                Storage::delete($symbol->images);
            }
            $images = $request->file('images')->store('public/upload');
        }

        $symbol->update([
            'images' => $images,
            'body' => request('body')
        ]);

        session()->flash('message', 'Successfully Updated');
        return redirect()->route('symbols.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Symbol $symbol)
    {
        $symbol->delete();
        session()->flash('message', 'Successfully Deleted');
        return view('backend.symbol.index');
    }
}
