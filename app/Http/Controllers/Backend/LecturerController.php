<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Lecturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LecturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.lecturer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.lecturer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'position' => 'required',
            'desc' => 'required',
            'image' => 'required'
        ]);

        $image = null;
        if ($request->hasFile('image')) {
            $image = $request->file('image')->store('public/upload');
        }

        Lecturer::create([
            'name' => request('name'),
            'position' => request('position'),
            'desc' => request('desc'),
            'image' => $image,
            'fb' => request('fb'),
            'ig' => request('ig'),
        ]);

        session()->flash('message', 'Successfully Added');
        return redirect()->route('lecturers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Lecturer $lecturer)
    {
        return view('backend.lecturer.edit', compact('lecturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lecturer $lecturer)
    {
        request()->validate([
            'name' => 'required',
            'position' => 'required',
            'desc' => 'required',
        ]);

        $image = $lecturer->image ?? null;
        if ($request->hasFile('image')) {
            if ($lecturer->image) {
                Storage::delete($lecturer->image);
            }
            $image = $request->file('image')->store('public/upload');
        }


        $lecturer->update([
            'name' => request('name'),
            'position' => request('position'),
            'desc' => request('desc'),
            'image' => $image,
            'fb' => request('fb'),
            'ig' => request('ig'),
        ]);

        session()->flash('message', 'Successfully Updated');
        return redirect()->route('lecturers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lecturer $lecturer)
    {
        $lecturer->delete();

        session()->flash('message', 'Successfully Delete');
        return view('backend.lecturer.index');
    }
}
