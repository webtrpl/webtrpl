<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Symbol;
use Illuminate\Http\Request;

class SymbolController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $symbol = Symbol::orderBy('created_at', 'desc');

        return datatables()->of($symbol)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            // ->addColumn('body', function (Post $post) {
            //     return "$post->body";
            // })
            ->addColumn('images', function (Symbol $symbol) {
                return '<img src="' . $symbol->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['images', 'action'])
            ->toJson();
    }
}
