<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Facility;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $facilities = Facility::orderBy('created_at', 'asc');

        return datatables()->of($facilities)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addColumn('image', function (Facility $facility) {
                return '<img src="' . $facility->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
