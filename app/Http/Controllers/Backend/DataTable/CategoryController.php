<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $categories = Category::orderBy('name', 'asc');

        return datatables()->of($categories)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addIndexColumn()
            ->toJson();
    }
}
