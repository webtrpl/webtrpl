<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //$users = User::orderBy('name', 'asc');
        $users = User::with('roles')->get();


        return datatables()->of($users)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addColumn('role', function ($users) {
                $role = $users->roles[0]['name'];
                return $role;
            })
            ->addIndexColumn()
            ->toJson();
    }
}
