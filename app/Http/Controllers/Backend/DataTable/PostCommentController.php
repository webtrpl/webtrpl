<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class PostCommentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $comments = Comment::orderBy('post_id', 'desc')->get();
        return datatables()->of($comments)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addIndexColumn()
            ->toJson();
    }
}
