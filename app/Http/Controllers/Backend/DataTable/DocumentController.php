<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $documents = Document::orderBy('created_at', 'desc');

        return datatables()->of($documents)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addIndexColumn()
            ->addColumn('created', function (Document $document) {
                return $document->created_at->format('d M Y');
            })
            ->toJson();
    }
}
