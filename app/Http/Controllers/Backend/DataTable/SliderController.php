<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $sliders = Slider::orderBy('created_at', 'desc');

        return datatables()->of($sliders)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addColumn('image', function (Slider $slider) {
                return '<img src="' . $slider->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
