<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $courses = Course::orderBy('semester', 'asc');

        return datatables()->of($courses)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addIndexColumn()
            ->toJson();
    }
}
