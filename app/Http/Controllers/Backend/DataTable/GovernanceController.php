<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Governance;
use Illuminate\Http\Request;

class GovernanceController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $governance = Governance::orderBy('period', 'desc');

        return datatables()->of($governance)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            // ->addColumn('body', function (Post $post) {
            //     return "$post->body";
            // })
            ->addColumn('image', function (Governance $governance) {
                return '<img src="' . $governance->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
