<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Vision;
use Illuminate\Http\Request;

class VisionController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $visions = Vision::orderBy('created_at', 'desc');

        return datatables()->of($visions)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            ->addColumn('image', function (Vision $vision) {
                return '<img src="' . $vision->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
