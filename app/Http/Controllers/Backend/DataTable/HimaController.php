<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Hima;
use Illuminate\Http\Request;

class HimaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $hima = Hima::orderBy('created_at', 'desc');

        return datatables()->of($hima)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            // ->addColumn('body', function (Post $post) {
            //     return "$post->body";
            // })
            ->addColumn('images', function (Hima $hima) {
                return '<img src="' . $hima->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['images', 'action'])
            ->toJson();
    }
}
