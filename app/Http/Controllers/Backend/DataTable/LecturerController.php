<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Lecturer;
use Illuminate\Http\Request;

class LecturerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $lecturers = Lecturer::orderBy('created_at', 'desc');

        return datatables()->of($lecturers)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            // ->addColumn('body', function (Post $post) {
            //     return "$post->body";
            // })
            ->addColumn('image', function (Lecturer $lecturer) {
                return '<img src="' . $lecturer->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
