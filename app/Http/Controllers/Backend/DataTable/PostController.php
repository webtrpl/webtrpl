<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $posts = Post::orderBy('created_at', 'desc');

        return datatables()->of($posts)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            // ->addColumn('body', function (Post $post) {
            //     return "$post->body";
            // })
            ->addColumn('user', function (Post $post) {
                return $post->user->name;
            })
            ->addColumn('image', function (Post $post) {
                return '<img src="' . $post->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
