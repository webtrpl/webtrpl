<?php

namespace App\Http\Controllers\Backend\DataTable;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $partners = Partner::orderBy('created_at', 'desc');

        return datatables()->of($partners)
            ->addColumn('action', 'backend.datatable-action.datatable-action')
            // ->addColumn('body', function (Post $post) {
            //     return "$post->body";
            // })
            ->addColumn('image', function (Partner $partner) {
                return '<img src="' . $partner->getImage() . '" height="120px"/>';
            })
            ->addIndexColumn()
            ->rawColumns(['image', 'action'])
            ->toJson();
    }
}
