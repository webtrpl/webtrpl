<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Hima;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HimaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.hima.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.hima.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'images' => 'required',
        ]);

        $images = null;
        if ($request->hasFile('images')) {
            $images = $request->file('images')->store('public/upload');
        }

        Hima::create([
            'images' => $images,
            'body' => request('body')
        ]);

        session()->flash('message', 'Successfully Added');
        return redirect()->route('himas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hima $hima)
    {
        return view('backend.hima.edit', compact('hima'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hima $hima)
    {
        $images = $hima->images ?? null;
        if ($request->hasFile('images')) {
            if ($hima->images) {
                Storage::delete($hima->images);
            }
            $images = $request->file('images')->store('public/upload');
        }

        $hima->update([
            'images' => $images,
            'body' => request('body')
        ]);

        session()->flash('message', 'Successfully Updated');
        return redirect()->route('himas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hima $hima)
    {
        $hima->delete();
        session()->flash('message', 'Successfully Deleted');
        return redirect()->route('himas.index');
    }
}
