<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.partner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required',
            'company' => 'required',
            'comment' => 'required',
            'city' => 'required',
            'field' => 'required',
            'email' => 'required|email',
            'website' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'photo' => 'required|mimes:jpg,jpeg,png'
        ]);

        $image = null;
        if ($request->hasfile('image')) {
            $image = $request->file('image')->store('public/upload');
        }

        $photo = null;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo')->store('public/upload');
        }

        Partner::create([
            'name' => request('name'),
            'company' => request('company'),
            'comment' => request('comment'),
            'city' => request('city'),
            'field' => request('field'),
            'email' => request('email'),
            'website' => request('website'),
            'image' => $image,
            'photo' => $photo
        ]);

        session()->flash('message', 'Successfully Added');

        return redirect()->route('partners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        return view('backend.partner.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        request()->validate([
            'name' => 'required',
            'company' => 'required',
            'comment' => 'required',
            'city' => 'required',
            'field' => 'required',
            'email' => 'required|email',
            'website' => 'required',
        ]);

        $image = $partner->image ?? null;
        if ($request->hasfile('image')) {
            if ($partner->image) {
                Storage::delete($partner->image);
            }
            $image = $request->file('image')->store('public/upload');
        }

        $photo = $partner->photo ?? null;
        if ($request->hasFile('photo')) {
            if ($partner->photo) {
                Storage::delete($partner->photo);
            }
            $photo = $request->file('photo')->store('public/upload');
        }

        $partner->update([
            'name' => request('name'),
            'company' => request('company'),
            'comment' => request('comment'),
            'city' => request('city'),
            'field' => request('field'),
            'email' => request('email'),
            'website' => request('website'),
            'image' => $image,
            'photo' => $photo
        ]);

        session()->flash('message', 'Successfully Updated');

        return redirect()->route('partners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        $partner->delete();

        session()->flash('message', 'Successfully Deleted');

        return redirect()->route('partners.index');
    }
}
