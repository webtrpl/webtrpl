<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\{Category, Comment, Course, Document, Facility, Governance, Hima, Lecturer, Post, Slider, Vision, Partner, Symbol};
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        $lecturers = Lecturer::orderBy('name', 'asc')->limit(8)->get();
        $sliders = Slider::latest()->get();
        $categories = Category::all();
        $posts = Post::orderBy('created_at', 'desc')->limit(3)->get();
        return view('frontend.index', compact('posts', 'categories', 'sliders', 'lecturers'));
    }

    public function singlepost($slug)
    {
        $sideposts = Post::orderBy('created_at', 'desc')->limit(10)->get();
        $post = Post::where('slug', '=', $slug)->first();
        $categories = Category::all();
        $comments = Comment::orderBy('created_at', 'desc')->limit(10)->get();
        return view('frontend.post.single', compact('post', 'sideposts', 'categories', 'comments'));
    }

    public function allpost()
    {
        $sideposts = Post::orderBy('created_at', 'desc')->limit(10)->get();
        $posts = Post::orderBy('created_at', 'desc')->paginate(4);
        if (empty($posts)) {
            return abort('404');
        }
        $categories = Category::all();
        return view('frontend.post.allpost', compact('posts', 'categories', 'sideposts'));
    }

    public function sidebar()
    {
    }

    public function course()
    {
        return view('frontend.course.index', [
            'sem1' => Course::where('semester', '1')->get(),
            'sem2' => Course::where('semester', '2')->get(),
            'sem3' => Course::where('semester', '3')->get(),
            'sem4' => Course::where('semester', '4')->get(),
            'sem5' => Course::where('semester', '5')->get(),
            'sem6' => Course::where('semester', '6')->get(),
            'sem7' => Course::where('semester', '7')->get(),
            'sem8' => Course::where('semester', '8')->get(),
        ]);
    }

    public function about()
    {
        $about = Vision::orderBy('id', 'desc')->first();
        return view('frontend.about.index', compact('about'));
    }

    public function governance()
    {
        $governance = Governance::orderBy('id', 'desc')->first();
        if (empty($governance)) {
            return abort('404');
        }
        return view('frontend.about.governance', compact('governance'));
    }

    public function symbol()
    {
        $symbol = Symbol::orderBy('id', 'desc')->first();
        if (empty($symbol)) {
            return abort('404');
        }
        return view('frontend.about.symbol', compact('symbol'));
    }

    public function partner()
    {
        $partners = Partner::orderBy('created_at', 'desc')->limit(6)->get();
        $testimonies = Partner::orderBy('comment', 'desc')->limit(3)->get();
        $posts = Post::all();
        $category = Category::where('name', '=', 'Kemitraan')->get();
        return view('frontend.partner.index', compact('category', 'partners', 'testimonies', 'posts'));
    }

    public function accreditation()
    {
        return view('frontend.accreditation.index');
    }

    public function download()
    {
        $perkuliahan = Document::where('category', 'perkuliahan')->get();
        $pengumuman = Document::where('category', 'pengumuman')->get();
        $skripsi = Document::where('category', 'skripsi')->get();
        $pkl = Document::where('category', 'pkl')->get();
        return view('frontend.student.download', compact('skripsi', 'pkl', 'pengumuman', 'perkuliahan'));
    }

    public function facility()
    {
        $images = Facility::all();
        $facilities = Facility::orderBy('created_at', 'asc')->get();
        return view('frontend.facility.index', compact('facilities', 'images'));
    }

    public function hima()
    {
        $hima = Hima::orderBy('created_at', 'desc')->first();
        if (empty($hima)) {
            return abort('404');
        }
        return view('frontend.student.hima', compact('hima'));
    }

    public function research()
    {
        $penelitians = Document::where('category', 'penelitian')->paginate(10);
        $pengabdians = Document::where('category', 'pengabdian')->paginate(10);
        $posts = Post::orderBy('created_at', 'asc')->get();
        $category_penelitian = Category::where('name', '=', 'Penelitian')->get();
        $category_pengabdian = Category::where('name', '=', 'Pengabdian')->get();
        return view('frontend.research.index', compact('penelitians', 'pengabdians', 'posts', 'category_penelitian', 'category_pengabdian'));
    }

    public function personil()
    {
        $plp = Lecturer::where([
            ['position', '!=', 'dosen'],
            ['position', '!=', 'kalab'],
            ['position', '!=', 'kaprodi'],
            ['position', '!=', 'mutu'],
            ['position', '!=', 'jurnal']
        ])->orderBy('name', 'asc')->get();

        $dosen = Lecturer::where([
            ['position', '!=', 'plp'],
            ['position', '!=', 'admin']
        ])->orderBy('name', 'asc')->get();

        return view('frontend.prodi.personil', compact('dosen', 'plp'));
    }
}
