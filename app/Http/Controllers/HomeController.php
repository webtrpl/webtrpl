<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Course;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backend.master');
    }

    public function dashboard()
    {
        $posts = Post::all();
        $users = User::all();
        $categories = Category::all();
        $courses = Course::all();
        return view('backend.dashboard', compact('posts', 'categories', 'users', 'courses'));
    }
}
