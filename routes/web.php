<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\Frontend\SiteController::class, 'index'])->name('root');
Route::get('/allpost', [App\Http\Controllers\Frontend\SiteController::class, 'allpost'])->name('allpost');
Route::get('/course', [App\Http\Controllers\Frontend\SiteController::class, 'course'])->name('course');
Route::get('/about', [App\Http\Controllers\Frontend\SiteController::class, 'about'])->name('about');
Route::get('/about/facility', [App\Http\Controllers\Frontend\SiteController::class, 'facility'])->name('facility');
Route::get('/partner', [App\Http\Controllers\Frontend\SiteController::class, 'partner'])->name('partner');
Route::get('/about/governance', [App\Http\Controllers\Frontend\SiteController::class, 'governance'])->name('governance');
Route::get('/about/symbol', [App\Http\Controllers\Frontend\SiteController::class, 'symbol'])->name('symbol');
Route::get('/academic/accreditation', [App\Http\Controllers\Frontend\SiteController::class, 'accreditation'])->name('accreditation');
Route::get('/student/download', [App\Http\Controllers\Frontend\SiteController::class, 'download'])->name('download');
Route::get('/student/hima', [App\Http\Controllers\Frontend\SiteController::class, 'hima'])->name('hima');
Route::get('/academic/research', [App\Http\Controllers\Frontend\SiteController::class, 'research'])->name('research');
Route::get('/academic/personil', [App\Http\Controllers\Frontend\SiteController::class, 'personil'])->name('personil');
Route::resource('comments', '\App\Http\Controllers\Backend\PostCommentController');

Route::get('/post/{slug}', [App\Http\Controllers\Frontend\SiteController::class, 'singlepost'])->name('singlepost');
Auth::routes([
    'register' => false,
    'verify' => true
]);

Route::middleware('has.role')->group(function () {
    Route::prefix('role-and-permission')->namespace('Permissions')->group(function () {
        Route::prefix('roles')->group(function () {
            Route::get('', [App\Http\Controllers\Permissions\RoleController::class, 'index'])->name('roles.index');
            Route::post('create', [App\Http\Controllers\Permissions\RoleController::class, 'store'])->name('roles.create');
            Route::get('{role}/edit', [App\Http\Controllers\Permissions\RoleController::class, 'edit'])->name('roles.edit');
            Route::put('{role}/edit', [App\Http\Controllers\Permissions\RoleController::class, 'update']);
            Route::get('{role}/delete', [App\Http\Controllers\Permissions\RoleController::class, 'delete'])->name('roles.delete');
        });
        Route::prefix('permissions')->group(function () {
            Route::get('', [App\Http\Controllers\Permissions\PermissionController::class, 'index'])->name('permissions.index');
            Route::post('create', [App\Http\Controllers\Permissions\PermissionController::class, 'store'])->name('permissions.create');
            Route::get('{permission}/edit', [App\Http\Controllers\Permissions\PermissionController::class, 'edit'])->name('permissions.edit');
            Route::put('{permission}/edit', [App\Http\Controllers\Permissions\PermissionController::class, 'update']);
            Route::get('{permission}/delete', [App\Http\Controllers\Permissions\PermissionController::class, 'delete'])->name('permissions.delete');
        });
        Route::get('assignable', [App\Http\Controllers\Permissions\AssignController::class, 'create'])->name('assign.create');
        Route::post('assignable', [App\Http\Controllers\Permissions\AssignController::class, 'store'])->name('assign.store');
        Route::get('assignable/{role}/edit', [App\Http\Controllers\Permissions\AssignController::class, 'edit'])->name('assign.edit');
        Route::put('assignable/{role}/update', [App\Http\Controllers\Permissions\AssignController::class, 'update'])->name('assign.update');
    });
    Route::prefix('admin')->group(function () {
        Route::group(['middleware' => ['permission:show posts']], function () {
            Route::resource('posts', '\App\Http\Controllers\Backend\PostController');
            Route::get('data/posts', '\App\Http\Controllers\Backend\DataTable\PostController')->name('data.posts');
        });
        Route::group(['middleware' => ['permission:show categories']], function () {
            Route::resource('categories', '\App\Http\Controllers\Backend\CategoryController');
            Route::get('data/categories', '\App\Http\Controllers\Backend\DataTable\CategoryController')->name('data.categories');
        });

        Route::group(['middleware' => ['permission:show courses']], function () {
            Route::resource('courses', '\App\Http\Controllers\Backend\CourseController');
            Route::get('data/courses', '\App\Http\Controllers\Backend\DataTable\CourseController')->name('data.courses');
        });

        Route::group(['middleware' => ['permission:show visions']], function () {
            Route::resource('visions', '\App\Http\Controllers\Backend\VisionController');
            Route::get('data/visions', '\App\Http\Controllers\Backend\DataTable\VisionController')->name('data.visions');
        });

        Route::group(['middleware' => ['permission:show sliders']], function () {
            Route::resource('sliders', '\App\Http\Controllers\Backend\SliderController');
            Route::get('data/sliders', '\App\Http\Controllers\Backend\DataTable\SliderController')->name('data.sliders');
        });
        Route::group(['middleware' => ['permission:show governances']], function () {
            Route::resource('governances', '\App\Http\Controllers\Backend\GovernanceController');
            Route::get('data/governances', '\App\Http\Controllers\Backend\DataTable\GovernanceController')->name('data.governances');
        });
        Route::group(['middleware' => ['permission:show symbols']], function () {
            Route::resource('symbols', '\App\Http\Controllers\Backend\SymbolController');
            Route::get('data/symbols', '\App\Http\Controllers\Backend\DataTable\SymbolController')->name('data.symbols');
        });
        Route::group(['middleware' => ['permission:show lecturers']], function () {
            Route::resource('lecturers', '\App\Http\Controllers\Backend\LecturerController');
            Route::get('data/lecturers', '\App\Http\Controllers\Backend\DataTable\LecturerController')->name('data.lecturers');
        });
        Route::group(['middleware' => ['permission:show partners']], function () {
            Route::resource('partners', '\App\Http\Controllers\Backend\PartnerController');
            Route::get('data/partners', '\App\Http\Controllers\Backend\DataTable\PartnerController')->name('data.partners');
        });
        Route::group(['middleware' => ['permission:show documents']], function () {
            Route::resource('documents', '\App\Http\Controllers\Backend\DocumentController');
            Route::get('data/documents', '\App\Http\Controllers\Backend\DataTable\DocumentController')->name('data.documents');
        });
        Route::group(['middleware' => ['permission:show facilities']], function () {
            Route::resource('facilities', '\App\Http\Controllers\Backend\FacilityController');
            Route::get('data/facilities', '\App\Http\Controllers\Backend\DataTable\FacilityController')->name('data.facilities');
        });
        Route::group(['middleware' => ['permission:show users']], function () {
            Route::resource('users', '\App\Http\Controllers\Backend\UserController');
            Route::get('data/users', '\App\Http\Controllers\Backend\DataTable\UserController')->name('data.users');
        });
        Route::group(['middleware' => ['permission:show himas']], function () {
            Route::resource('himas', '\App\Http\Controllers\Backend\HimaController');
            Route::get('data/himas', '\App\Http\Controllers\Backend\DataTable\HimaController')->name('data.himas');
        });
        Route::group(['middleware' => ['permission:show comments']], function () {
            Route::resource('comments', '\App\Http\Controllers\Backend\PostCommentController');
            Route::get('data/comments', '\App\Http\Controllers\Backend\DataTable\PostCommentController')->name('data.comments');
        });
    });
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('home')->middleware('verified');
