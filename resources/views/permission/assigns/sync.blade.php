@extends('backend.master')
@section('content')

    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Sync Permission</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <form method="post" action="{{ route('assign.update', $role) }}">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-sm-12">
                <!-- select -->
                    <div class="form-group">
                        <label for="role">Select Role</label>
                        <select name="role" id="role" class="form-control @error('permission') is-invalid  @enderror">
                            <option disabled selected>Choose</option>
                            @foreach ($roles as $item)
                                <option {{ $role->id == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('role')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                <!-- Select multiple-->
                    <div class="form-group">
                        <label for="permissions">Select Permissions</label>
                        <select name="permissions[]" id="permissions" multiple class="form-control select2 @error('permissions') is-invalid  @enderror">
                            @foreach ($permissions as $permission)
                                <option {{ $role->permissions()->find($permission->id) ? 'selected' : '' }} value="{{ $permission->name }}">{{ $permission->name }}</option>
                            @endforeach
                        </select>
                        @error('permissions')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Assign</button>
            </form>
        </div>
        <!-- /.card-body -->
    </div>    

@stop


@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
@endpush