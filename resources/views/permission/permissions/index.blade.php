@extends('backend.master')
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-success">
                <div class="card-header">
                <h3 class="card-title">Create a new Permission</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('permissions.create') }}">
                @csrf    
                @include('permission.permissions.partials.form-control', ['submit' => 'Create'])
                <!-- /.card-body -->
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">Permissions</h3>

            <div class="card-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                <div class="input-group-append">
                    <button type="submit" class="btn btn-default">
                    <i class="fas fa-search"></i>
                    </button>
                </div>
                </div>
            </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
            <table class="table table-hover text-nowrap">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Guard Name</th>
                    <th>Create At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($permissions as $permission)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $permission->name }}</td>
                    <td>{{ $permission->guard_name }}</td>
                    <td>{{ $permission->created_at->format("d F Y") }}</td>
                    <td> <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-primary btn-sm">Edit</a> 
                        <a href="{{ route('permissions.delete', $permission) }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm">Delete</a></td>
                </tr>
                @endforeach
            </tbody>
            </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
    </div>
@stop