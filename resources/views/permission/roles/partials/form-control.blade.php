<div class="card-body">
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control" placeholder="Type a Name" value="{{ old('name') ?? $role->name }}">
    </div>
    <div class="form-group">
        <label for="guard_name">Guard Name</label>
        <input name="guard_name" type="text" class="form-control" placeholder='Default to "web"' value="{{ old('guard_name') ?? $role->guard_name }}">
    </div>
    <button type="submit" class="btn btn-primary">{{ $submit }}</button>
</div>