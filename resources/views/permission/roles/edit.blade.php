@extends('backend.master')
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Edit Role</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="#">
                @csrf   
                @method('PUT')
                @include('permission.roles.partials.form-control')
                <!-- /.card-body -->
                </form>
            </div>
        </div>
    </div>
@stop