<div id="sidebar" class="col-md-4 col-sm-4 col-xs-12">
    <div class="widget">
        <div class="widget-title">
            <h4>Related Post</h4>
            <hr>
        </div>
        <div>
            <ul class="popular-courses">
                @foreach ($sideposts as $p)
                <li>
                    <a href="{{ route('singlepost', $p->slug) }}"><img src="{{ asset('/storage/'.$p->image) }}" alt="" class="alignleft img-thumbnail"><strong> {{ $p->title }}</strong></a>
                    <p>{{ $p->created_at->format("d M Y") }}</p>
                </li>    
                @endforeach
                {{-- <li>Post 1</li>
                <li>Post 2</li> --}}
            </ul>
        </div>
    </div>
</div>