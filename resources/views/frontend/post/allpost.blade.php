@extends('frontend.layouts.master')
@section('content')

<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/trpl.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Informasi Terkini Seputar Kampus</h4>
                    </div>
                </div>
            </div>
        </div>
</section>

<section class="grey section">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-8 col-sm-8 col-xs-12">
                <div class="blog-wrapper">
                    <div class="row second-bread">
                        <div class="col-md-6 text-left">
                            <h1>Blog & Berita Seputar Kampus</h1>
                        </div>
                    </div>
                </div>
                @foreach($posts as $post)
                <div class="blog-wrapper">
                    <div class="blog-title">
                        <a class="category_title" href="#" title="">{{ $post->category }}</a>
                        <h2><a href="{{ route('singlepost', $post->slug) }}" title="">{{ $post->title }}</a></h2>
                        <div class="post-meta">
                            <span>
                            <i class="fa fa-user"></i>
                            <a href="#">{{ $post->user->name}}</a>
                            </span>
                            <span>
                            <i class="fa fa-tag"></i>
                                @foreach ($categories as $category)  
                                    @if($post->categories->contains($category))    
                                        <a href="#">{{ $category->name }}</a>
                                    @endif
                                @endforeach
                            </span>
                        </div>
                    </div>
                    <div class="blog-image">
                        <a href="{{ route('singlepost', $post->slug) }}" title=""><img src="{{ asset('storage/'.$post->image) }}" alt="" class="img-responsive"></a>
                    </div>
                    <div class="blog-desc">
                        <div class="post-date">
                            <span class="day">{{ $post->created_at->format('d') }}</span>
                            <span class="month">{{ $post->created_at->format('M') }}</span>
                        </div>
                        <p>{!! (str_word_count($post->body) > 60 ? substr($post->body,0,200).'...' : $post->body) !!}</p>
                        <div style="float: right">
                            @if (str_word_count($post->body) > 60)
                                <a href="{{ route('singlepost', $post->slug) }}" class="btn btn-info">Baca selengkapnya</a>
                            @endif
                        </div>
                        <br>
                    </div>
                </div>
                @endforeach
                <hr class="invis">
                <nav class="text-center">
                    {{ $posts->links() }}
                </nav>
            </div>
            @include('frontend.post.sidebar')
        </div>
    </div>
</section>

@stop