@extends('frontend.layouts.master')
@section('content')

<section class="grey section">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-8 col-sm-8 col-xs-12">
                {{-- <div class="blog-wrapper">
                    <div class="row second-bread">
                        <div class="col-md-12 text-left">
                            <h1>Blog & News</h1>
                        </div>
                    </div>
                </div> --}}
                <div class="blog-wrapper single-blog-wrapper">
                    <div class="blog-title">
                        <h2>{{ $post->title }}</h2>
                        <div class="post-meta">
                            <span>
                            <i class="fa fa-user"></i>
                            <a href="#">{{ $post->user->name}}</a>
                            </span>
                            <span>
                            {{-- <i class="fa fa-tag"></i>
                            <a href="#">Galleries</a>
                            </span>
                            <span>
                            <i class="fa fa-comments"></i>
                            <a href="#">21 Comments</a>
                            </span>
                            <span>
                            <i class="fa fa-eye"></i>
                            <a href="#">123 Views</a>
                            </span>
                            <span> --}}
                            <i class="fa fa-clock-o"></i>
                            <a href="#">{{ $post->created_at->format('d M Y') }}</a>
                            </span>
                            
                        </div>  
                    </div>
                    <div class="blog-image">
                        <img src="{{ asset('storage/'.$post->image) }}" alt="" class="img-responsive">
                    </div>
                    <div class="blog-desc">
                        <p>{!! $post->body !!}</p>
                        <hr class="invis">
                        <div class="tags">
                            @foreach ( $categories as $category)  
                                @if($post->categories->contains($category))    
                                    <a href="#" title="" >{{ $category->name }}</a>    
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

                <hr class="invis">
                <div class="blog-wrapper comment-wrapper">
                    <div id="reviews" class="feedbacks">
                        <h3>
                        What others say about this post? 
                        </h3>
                        <div>
                            <div class="well">
                                @foreach ($comments->reverse() as $comment)
                                @if ( $post->id == ($comment->post_id))
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <img class="media-object" src="{{ asset('storage/public/default.png')}}" alt="Generic placeholder image">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{ $comment->name }}</h4>
                                        <div class="time-comment clearfix">
                                            <small class="pull-left">{{ $comment->created_at->diffForHumans() }}</small>
                                            {{-- <a class="pull-right btn btn-primary btn-xs">Reply</a> --}}
                                        </div>
                                        <p>{!! $comment->comment !!}</p>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                                <hr>
                                {{-- <div class="media comment-reply">
                                    <div class="media-left">
                                        <a href="#">
                                        <img class="media-object" src="upload/xstudent_02.png.pagespeed.ic.y-PM-y4pVj.png" alt="Generic placeholder image">
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Amanda FOXOE</h4>
                                        <div class="time-comment clearfix">
                                            <small class="pull-left">21 Jun 2015</small>
                                            <a class="pull-right btn btn-primary btn-xs">Reply</a>
                                        </div>
                                        <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                                    </div>
                                </div> --}}
                                
                                <div class="content-widget">
                                    <div class="widget-title">
                                        <h4>Leave a Comment</h4>
                                        <hr>
                                    </div>
                                    <div class="commentform">
                                        {{-- <p>Please use only default html tags.</p> --}}
                                        <form method="POST" action="{{ route('comments.store') }}">
                                            @csrf
                                            <input type="hidden" name="post_id" value="{{$post->id}}">
                                            <input name="name" type="text" class="form-control" placeholder="Your name">
                                            <input name="email" type="email" class="form-control" placeholder="Your email">
                                            <textarea name="comment" type="text" class="form-control" placeholder="Message goes here"></textarea>
                                            <input type="submit" value="Send Comment" class="btn btn-primary btn-block"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

            @include('frontend.post.sidebar')

        </div>
    </div>
</section>

@stop
{{-- 

@push('script')
<script src="{{ asset('frontend/js/jquery.min.js.pagespeed.jm.iDyG3vc4gw.js') }}"></script>
<script src="{{ asset('frontend/js/bootstrap.min.js%2bretina.js%2bwow.js.pagespeed.jc.pMrMbVAe_E.js') }}"></script><script>eval(mod_pagespeed_gFRwwUbyVc);</script>
<script>eval(mod_pagespeed_rQwXk4AOUN);</script>
<script>eval(mod_pagespeed_U0OPgGhapl);</script>
<script src="{{ asset('frontend/js/carousel.js%2bcustom.js.pagespeed.jc.nVhk-UfDsv.js') }}"></script><script>eval(mod_pagespeed_6Ja02QZq$f);</script>
<script>eval(mod_pagespeed_KxQMf5X6rF);</script>
@endpush --}}

