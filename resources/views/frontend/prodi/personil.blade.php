@extends('frontend.layouts.master')
@section('slider')

<!-- Halaman dosen -->
<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h4>Tim Dosen TRPL</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="team-members row">
                        @foreach($dosen as $dos)
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <br>
                            <div class="team">
                                <img src="{{ asset('storage/'.$dos->image) }}" alt="" class="img-responsive wow fadeInUp">
                                <div class="team-hover-content">
                                    <h5>{{ $dos->name }}</h5>
                                    <span>{{ $dos->position }}</span>
                                    <p>{!! $dos->desc !!}</p>
                                    <div class="social">
                                        <a href="{{ $dos->fb }}" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="{{ $dos->ig }}" title="Instagram"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End halaman dosen -->

<!-- Halaman plp -->
<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h4>PLP dan Administrasi</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="team-members row">
                        @foreach($plp as $p)
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <br>
                            <div class="team">
                                <img src="{{ asset('storage/'.$p->image) }}" alt="" class="img-responsive wow fadeInUp">
                                <div class="team-hover-content">
                                    <h5>{{ $p->name }}</h5>
                                    <span>{{ $p->position }}</span>
                                    <p>{!! $p->desc !!}</p>
                                    <div class="social">
                                        <a href="{{ $p->fb }}" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        <a href="{{ $p->ig }}" title="Instagram"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End halaman plp -->
@endsection

