@extends('frontend.layouts.master')
@section('content')
<section class="grey page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <h1>Profile</h1>
            </div>
        </div>  
    </div>
</section>

<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="media-element">
                    <div id="myCarousel" class="carousel" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="{{ asset('storage/'.$about->image) }}" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                    <h4>Sambutan Ketua Program Studi</h4>
                    <hr>
                    </div>
                        <p>{!! $about->welcome !!}</p>
                    <div class="row">
                        <div class="col-md-6">
                            {{-- <ul class="check">
                                <li>Professional Teachers</li>
                                <li>24/7 Online Course</li>
                                <li>Works with All Devices</li>
                                <li>Friendly User Panel</li>
                                <li>Support Desk for All Prices</li>
                            </ul>
                            </div>
                            <div class="col-md-6">
                            <ul class="check">
                                <li>Lifetime Membership</li>
                                <li>Online Chat with Support</li>
                                <li>Second Course 50% Cheap</li>
                                <li>Limitless Bandwidth</li>
                                <li>And much more...</li>
                            </ul>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        <hr class="invis1">
        <div class="row">
            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                    <h4>Visi dan Misi</h4>
                    <hr>
                    </div>
                    <div class="accordion-widget">
                        <div class="accordion-toggle-2">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour">
                                                <h3>Visi <i class="indicator fa fa-minus"></i></h3>
                                            </a>    
                                        </div>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <p>{!! $about->vision !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive">
                                            <h3>Misi <i class="indicator fa fa-plus"></i></h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>{!! $about->mission !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix">
                                                <h3>Tujuan <i class="indicator fa fa-plus"></i></h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>{!! $about->goal !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                        <h4>Sasaran</h4>
                        <hr>
                    </div>
                    <div>
                        <p>{!! $about->target !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@stop            