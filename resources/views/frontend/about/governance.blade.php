@extends('frontend.layouts.master')
@section('content')

<section class="grey section">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-12 col-sm-12 col-xs-12">
                <div class="blog-wrapper">
                    <div class="row second-bread">
                        <div class="col-md-12 text-left">
                            <h1>Pengurus Program Studi Teknologi Rekayasa Perangkat Lunak</h1>
                            <p>{{ $governance->period}}</p>
                        </div>
                    </div>
                </div>
                <div class="blog-wrapper">
                    <div class="blog-image">
                        <img src="{{ asset('storage/'.$governance->image) }}" alt="" class="img-responsive">
                    </div>
                    <div class="blog-desc">
                        <p>{!! $governance->body !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop