@extends('frontend.layouts.master')
@section('content')
<section class="grey page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <h1>Kurikulum</h1>
            </div>
        </div>  
    </div>
</section>

<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="widget-title">
                        <h4>Kurikulum Prodi TRPL 2021</h4>
                        <hr>
                    </div>
                    <div class="tabbed-widget">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Semester 1</a></li>
                            <li><a data-toggle="tab" href="#menu2">Semester 2</a></li>
                            <li><a data-toggle="tab" href="#menu3">Semester 3</a></li>
                            <li><a data-toggle="tab" href="#menu4">Semester 4</a></li>
                            <li><a data-toggle="tab" href="#menu5">Semester 5</a></li>
                            <li><a data-toggle="tab" href="#menu6">Semester 6</a></li>
                            <li><a data-toggle="tab" href="#menu7">Semester 7</a></li>
                            <li><a data-toggle="tab" href="#menu8">Semester 8</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem1 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem2 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem3 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu4" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem4 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu5" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem5 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu6" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem6 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu7" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem7 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="menu8" class="tab-pane fade">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>Jumlah SKS</th>
                                            <th>Deskripsi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sem8 as $sem)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $sem->code }}</td>
                                            <td>{{ $sem->name }}</td>
                                            <td>{{ $sem->credit }}</td>
                                            <td>{!! $sem->description !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop