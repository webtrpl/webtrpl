@extends('frontend.layouts.master')
@section('content')

<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/trpl.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Pencapaian Program Studi</h4>
                    </div>
                </div>
            </div>
        </div>
</section>


<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                {{-- <h4>Akreditasi</h4> --}}
                <p>Dokumen Akreditasi Program Studi dan Perguruan Tinggi</p>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-responsive">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Dokumen Akreditasi</th>
                        <th scope="col">No. SK</th>
                        <th scope="col">Peringkat</th>
                        <th scope="col">Unduh</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Politeknik Pertanian Negeri Samarinda</td>
                        <td>201/BAN-PT/Akred/2019</td>
                        <td>Baik Sekali</td>
                        <td>SK dan Sertifikat</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>D-IV Teknologi Rekayasa Perangkat Lunak</td>
                        <td>201/BAN-PT/Akred/2019</td>
                        <td>C</td>
                        <td>SK dan Sertifikat</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

@stop