@extends('frontend.layouts.master')
@section('content')
<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/furniture.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    {{-- <div class="overlay"></div> --}}
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Fasilitas Kampus</h4>
                    </div>
                </div>
            </div>
        </div>
        
</section>

<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                        <h4>Fasilitas Umum</h4>
                        <hr>
                    </div>
                    <div class="media-element">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="{{ asset('storage/public/sasana.jpg') }}" alt="" class="img-responsive">
                        </div>
                        @foreach ($images as $image)   
                        <div class="item">
                            <img src="{{ asset('storage/'.$image->image) }}" alt="" class="img-responsive">
                            {{-- <div class="carousel-caption-inner d-none d-md-block">
                                <p>{{ $facility->name }}</p>
                            </div> --}}
                        </div>
                        @endforeach
                    </div>
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                        <h4>Deskripsi</h4>
                        <hr>    
                    </div>
                    <div class="accordion-widget">
                        <div class="accordion-toggle-2">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    @foreach ($facilities as $f)
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#{{$f->id }}">
                                                <h3>{{ $f->name }} <i class="indicator fa fa-plus"></i></h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="{{ $f->id }}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <p>{!! $f->description !!}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop


