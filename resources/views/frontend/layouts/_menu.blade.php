<header class="header">
    <div class="container">
        <div class="hovermenu ttmenu">
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                <div class="logo">
                    <a class="navbar-brand" href="#"><img src="#" alt=""></a>
                </div>
            </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="ttmenu-half"><a href="{{ route('root') }}">Home</a>
                </li>
                </li>
                    <li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Profile<b class="fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="ttmenu-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <ul>
                                                    <li><a href="{{ route('about') }}">Visi Misi</a></li>
                                                    <li><a href="{{ route('governance') }}">Pengurus Prodi</a></li>
                                                    <li><a href="{{ route('symbol') }}">Makna Lambang</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Pojok Prodi<b class="fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="ttmenu-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <ul>
                                                    {{-- <li><a href="{{ route('accreditation')}}">Akreditasi</a></li> --}}
                                                    <li><a href="{{ route('research') }}">Riset dan Pengabdian</a></li>
                                                    <li><a href="{{ route('partner') }}">Kemitraan</a></li>
                                                    <li><a href="{{ route('facility') }}">Fasilitas Umum</a></li>
                                                    <li><a href="{{ route('personil') }}">Dosen dan PLP</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown ttmenu-half"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Mahasiswa <b class="fa fa-angle-down"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="ttmenu-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box">
                                                <ul>
                                                    <li><a href="{{ route('hima') }}">Himpunan Mahasiswa</a></li>
                                                    <li><a href="{{ route('download') }}">Informasi Akademik</a></li>
                                                    <li><a href="http://tracer.politanisamarinda.ac.id">Tracer Study</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    
                    <li><a href="{{ route('allpost') }}">Seputar Kampus</a></li>
                    <li><a href="{{ route('course') }}">Lihat Kurikulum</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a class="btn btn-primary" href="http://pmb.politanisamarinda.ac.id/"><i class="fa fa-sign-in"></i> Daftar Sekarang</a></li>
            </ul>
        </div>
    </div>
</div>
</div>
</header>