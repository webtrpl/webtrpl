<footer class="dark footer section">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-md-6 col-xs-12">
                <div class="widget">
                    <div class="widget-title">
                        <h4>Program Studi TRPL</h4>
                    <hr>
                    </div>
                    <p>Program Studi Teknologi Rekayasa Perangkat Lunak (TRPL) berada di Lingkungan Kampus Politeknik Pertanian Negeri Samarinda</p>
                    <p>Jln. Sam Ratulangi, Kel. Gunung Panjang, Kec. Samarinda Seberang, Kota Samarinda, Kalimantan Timur, Kode Pos 75131</p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="widget-title">
                    <h4>Peta Kampus</h4>
                <hr>
                </div>
                <div id="mapid" style="height: 60vh;"></div>
                {{-- <div id="googleMap" style="width:100%;height:380px;"></div> --}}
            </div>
            <div class="col-md-3 col-md-6 col-xs-12">
                <div class="widget">
                <div class="widget-title">
                    <h4>Link Terkait</h4>
                    <hr>
                </div>
                <ul class="contact-details">
                    <li><i class="fa fa-link"></i> <a href="https://politanisamarinda.ac.id" target="_blank">Politani Samarinda</a></li>
                    <li><i class="fa fa-link"></i> <a href="http://sister.politanisamarinda.ac.id/auth/login" target="_blank">Sister Politani Samarinda</a></li>
                    <li><i class="fa fa-link"></i> <a href="https://sinta.ristekbrin.go.id/" target="_blank">Sinta</a></li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<section class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
                <p><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></p>
            </div>
            <div class="col-md-6 text-right">
                <ul class="list-inline">
                    <li><a href="#">Terms of Usage</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Sitemap</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
</div>

@push('script')
<script>
var mapOptions = {
		center: [-0.5354591395414938, 117.12430994535454],
		zoom: 13
	}

var mymap = new L.map('mapid', mapOptions);
var marker = L.marker([-0.5354591395414938, 117.12430994535454]).addTo(mymap);
    marker.bindPopup("<b>Teknologi Rekayasa Perangkat Lunak</b><br>Politeknik Pertanian Negeri Samarinda").openPopup();
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    //accessToken: 'pk.eyJ1IjoiYXNlcDg5IiwiYSI6ImNrcnY3NXU5ajAzeDMyb29lNXN2cjh5ZWsifQ.PBIUqPaYyLF7OC3zwGUDhQ'
    accessToken: 'pk.eyJ1IjoiYXNlcDg5IiwiYSI6ImNrcnY3MnhwYzAzcmEyb24wbzVhZXdnNzAifQ.D5cWKFKB_HXwfp7Rozsz8w'
}).addTo(mymap);
</script>



{{-- <script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
function initialize() {
    var propertiPeta = {
        center:new google.maps.LatLng(-0.537178,117.124821),
        zoom:9,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    
    var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
    
    // membuat Marker
    var marker=new google.maps.Marker({
        position: new google.maps.LatLng(-0.537178,117.124821),
        map: peta
    });

}


google.maps.event.addDomListener(window, 'load', initialize);
</script> --}}

@endpush


