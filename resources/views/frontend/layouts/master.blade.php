@include('frontend.layouts._header')
<body>

    @include('frontend.layouts._loading')
    <div id="wrapper">
        @include('frontend.layouts._login')
        @include('frontend.layouts._menu')

    @yield('slider')
    @yield('content')

    @include('frontend.layouts._footer')
    @include('frontend.layouts._script')

</body>
</html>