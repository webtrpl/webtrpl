@extends('frontend.layouts.master')
@section('content')

<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="widget-title">
                    <h4>Unduh Dokumen</h4>
                    <hr>
                    </div>
                    <div class="tabbed-widget">
                        <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Penelitian</a></li>
                        <li><a data-toggle="tab" href="#menu1">Pengabdian</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <ol>
                                @foreach ($penelitians as $penelitian)
                                    <li><a href="{{ $penelitian->getDocument() }}" target="_blank">{{ $penelitian->name }}</a></li>
                                @endforeach
                                </ol>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <ol>
                                @foreach ($pengabdians as $pengabdian)
                                    <li><a href="{{ $pengabdian->getDocument() }}" target="_blank">{{ $pengabdian->name }}</a></li>
                                @endforeach
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/microscope.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Berita Penelitian</h4>
                    </div>
                </div>
            </div>
        </div>
</section> 
<section class="grey section">
    <div class="container">
        <div class="row blog-widget">
            @foreach( $posts->reverse() as $post )
            @if($post->categories[0]->name == "Penelitian")
            <div class="col-md-4 col-sm-6">
                <div class="blog-wrapper">
                    <div class="blog-title">
                        <a class="category_title" href="#" title=""></a>
                        <h2><a href="single.html" title="">{{ $post->title }}</a></h2>
                        <div class="post-meta">
                            <span>
                                <i class="fa fa-user"></i>
                                <a href="#">{{ $post->user->name }}</a>
                            </span>
                        </div>
                    </div>
                    <div class="blog-image">
                        <a href="{{ route('singlepost', $post->slug) }}" title=""><img src="{{ asset('storage/'.$post->image) }}" class="img-responsive"></a>
                    </div>
                    <div class="blog-desc">
                        <p>{!! (str_word_count($post->body) > 60 ? substr($post->body,0,200).'...' : $post->body) !!}</p>
                        @if (str_word_count($post->body) > 60)
                            <a href="{{ route('singlepost', $post->slug) }}" class="btn btn-info">Baca selengkapnya</a>
                        @endif
                    </div>
                </div>
            </div>   
            @endif  
            @endforeach
        </div>
    </div>
</section>
<!-- End Section untuk berita -->

<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/bus.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Berita Pengabdian Masyarakat</h4>
                    </div>
                </div>
            </div>
        </div>
</section> 
<section class="grey section">
    <div class="container">
        <div class="row blog-widget">
            @foreach( $posts->reverse() as $post )
            @if($post->categories[0]->name == "Pengabdian")
            <div class="col-md-4 col-sm-6">
                <div class="blog-wrapper">
                    <div class="blog-title">
                        <a class="category_title" href="#" title=""></a>
                        <h2><a href="single.html" title="">{{ $post->title }}</a></h2>
                        <div class="post-meta">
                            <span>
                                <i class="fa fa-user"></i>
                                <a href="#">{{ $post->user->name }}</a>
                            </span>
                        </div>
                    </div>
                    <div class="blog-image">
                        <a href="{{ route('singlepost', $post->slug) }}" title=""><img src="{{ asset('storage/'.$post->image) }}" class="img-responsive"></a>
                    </div>
                    <div class="blog-desc">
                        <p>{!! (str_word_count($post->body) > 60 ? substr($post->body,0,200).'...' : $post->body) !!}</p>
                        @if (str_word_count($post->body) > 60)
                            <a href="{{ route('singlepost', $post->slug) }}" class="btn btn-info">Baca selengkapnya</a>
                        @endif
                    </div>
                </div>
            </div>   
            @endif  
            @endforeach
        </div>
        
    </div>
</section>
<!-- End Section untuk berita -->
@stop