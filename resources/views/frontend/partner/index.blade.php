@extends('frontend.layouts.master')
@section('content')
<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/night.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Progam Kemitraan Prodi TRPL</h4>
                        <p>Praktik Kerja dan Magang</p>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                <h4>Mitra Institusi</h4>
                <p>Kami berkolaborasi untuk terus meningkatkan kualitas</p>
                </div>
            </div>
        </div>
        
        <div class="row">
            @foreach ($partners as $partner)
            <div class="col-md-2">
                <img src="{{ asset('/storage/'.$partner->image) }}" alt="" class="img-responsive img-thumbnail">
            </div>
            @endforeach
        </div>
    </div>
</section>


<section class="blue page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 style="color: white">Bagaimana pendapat mereka ?</h1>
            </div>
        </div>  
    </div>
</section>

<section class="white section">
    <div class="container">
        {{-- <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h4>Happy Students</h4>
                    <p>What Our Students Say About LearnPLUS</p>
                </div>
            </div>
        </div> --}}
        <div class="row">
            @foreach ($testimonies as $testi)    
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="testimonial">
                    <img class="alignleft img-circle" src="{{ asset('/storage/'.$testi->photo) }}" alt="">
                    <p>{!! $testi->comment !!}</p>
                    <div class="testimonial-meta">
                        <h4>{{ $testi->name}} <small><a href="#"> {{ $testi->website }} </a></small></h4>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
</section>


{{-- <section class="blue section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-left">
                <h4 style="color: white">Kurikulum 2021</h4><hr>
                <p style="color: white; font-family:Arial, Helvetica, sans-serif; font-style:normal">{{config('trpl.kurikulum')}}.
                </p>
                <p style="text-align: center; font-family:Arial, Helvetica, sans-serif; font-style:normal "><a class="btn btn-danger" href="{{ route('course') }}"><i class="fa fa-sign-in"></i> Lihat kurikulum 2021 selengkapnya</a></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="why-us text-right wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <i class="fa fa-user alignright"></i>
                    <h4>Dosen Kompeten</h4>
                    <p>Disini kamu akan bertemu dengan dosen-dosen yang kompeten dibidangnya dan juga dosen industri yang telah bekerjasama supaya kamu dapat pengalaman yang luar biasa.</p>
                </div>  
            </div>
        </div>
    </div>
</section>
--}}

<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/meeting.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Berita Kemitraan</h4>
                    </div>
                </div>
            </div>
        </div>
</section> 

<!-- Section untuk berita mulai dari sini -->
<section class="grey section">
    <div class="container">
        <div class="row blog-widget">
            @foreach( $posts as $post )
            {{-- {{ $post->categories[0]->name }} --}}
            @if($post->categories[0]->name == "Kemitraan")
            <div class="col-md-4 col-sm-6">
                <div class="blog-wrapper">
                    <div class="blog-title">
                        <a class="category_title" href="#" title=""></a>
                        <h2><a href="single.html" title="">{{ $post->title }}</a></h2>
                        <div class="post-meta">
                            <span>
                                <i class="fa fa-user"></i>
                                <a href="#">{{ $post->user->name }}</a>
                            </span>
                        </div>
                    </div>
                    <div class="blog-image">
                        <a href="{{ route('singlepost', $post->slug) }}" title=""><img src="{{ asset('storage/'.$post->image) }}" class="img-responsive"></a>
                    </div>
                    <div class="blog-desc">
                        <p>{!! (str_word_count($post->body) > 60 ? substr($post->body,0,200).'...' : $post->body) !!}</p>
                        @if (str_word_count($post->body) > 60)
                            <a href="{{ route('singlepost', $post->slug) }}" class="btn btn-info">Baca selengkapnya</a>
                        @endif
                    </div>
                </div>
            </div>   
            @endif  
            @endforeach
        </div>
    </div>
</section>
<!-- End Section untuk berita -->

@endsection