@extends('frontend.layouts.master')
@section('content')
<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/iphone.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    {{-- <div class="overlay"></div> --}}
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Pengumuman dan Unduh Dokumen</h4>
                        <p>Informasi dan Dokumen Akademik</p>
                    </div>
                </div>
            </div>
        </div>
        
</section>
<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="widget-title">
                    <h4>Pengumuman</h4>
                    <hr>
                </div>
                <div>
                    <table class="table table-hover table-responsive">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Dokumen</th>
                            <th scope="col">Perihal</th>
                            <th scope="col">Tanggal Unggah</th>
                            <th scope="col">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach ($pengumuman as $umum)
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $umum->name }}</td>
                            <td>{{ $umum->subject }}</td>
                            <td>{{ $umum->created_at }}</td>
                            <td><i><a href="{{ $umum->getDocument() }}">unduh</a></i></td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="widget-title">
                        <h4>Unduh Dokumen</h4>
                        <hr>    
                    </div>
                    <div class="accordion-widget">
                        <div class="accordion-toggle-2">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour">
                                            <h3>Dokumen Perkuliahan <i class="indicator fa fa-plus"></i></h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ol>
                                                @foreach ($perkuliahan as $kuliah)
                                                <li><a href="{{ $kuliah->getDocument() }}" target="_blank">{{ $kuliah->name }}</a></li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive">
                                            <h3>Praktek Kerja Lapang <i class="indicator fa fa-plus"></i></h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ol>
                                                @foreach ($pkl as $p)
                                                <li><a href="{{ $p->getDocument() }}" target="_blank">{{ $p->name }}</a></li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix">
                                            <h3>Skripsi <i class="indicator fa fa-plus"></i></h3>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ol>
                                                @foreach ($skripsi as $s)
                                                <li><a href="{{ $s->getDocument() }}" target="_blank">{{ $s->name }}</a></li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


{{-- <section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                    <h4>Praktek Kerja Lapang</h4>
                    <hr>
                    </div>
                    <div class="tabbed-widget">
                        <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Syarat</a></li>
                        <li><a data-toggle="tab" href="#menu1">Dokumen</a></li>
                        <li><a data-toggle="tab" href="#menu2">Menu 3</a></li>
                        <li><a data-toggle="tab" href="#menu3">Menu 4</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                            </div>
                            <div id="menu3" class="tab-pane fade">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="content-widget">
                    <div class="widget-title">
                    <h4>Skripsi</h4>
                    <hr>
                    </div>
                    <div class="tabbed-widget">
                        <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home">Menu 1</a></li>
                        <li><a data-toggle="tab" href="#menu1">Menu 2</a></li>
                        <li><a data-toggle="tab" href="#menu2">Menu 3</a></li>
                        <li><a data-toggle="tab" href="#menu3">Menu 4</a></li>
                        </ul>
                        <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
@stop