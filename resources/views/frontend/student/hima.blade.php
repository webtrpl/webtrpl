@extends('frontend.layouts.master')
@section('content')

<section class="grey section">
    <div class="container">
        <div class="row">
            <div id="content" class="col-md-12 col-sm-12 col-xs-12">
                <div class="blog-wrapper">
                    <div class="row second-bread">
                        <div class="col-md-12 text-left">
                            <h1>Himpunan Mahasiswa</h1>
                        </div>
                    </div>
                </div>
                <div class="blog-wrapper">
                    <div class="blog-image">
                        <a href="#" title="">
                            <img src="{{ $hima->getImage() }}" class="img-responsive">
                        </a>
                    </div>
                    <div class="blog-desc">
                        <p>{!! $hima->body !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop