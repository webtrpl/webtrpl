@extends('frontend.layouts.master')
@section('slider')
<section class="slider-section">
    <div class="tp-banner-container">
        <div class="tp-banner">
            @foreach ($sliders as $slider)
            <ul>
                {{-- <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="#" data-saveperformance="off" data-title="Slide">
                    <img src="{{ asset('storage/'.$slider->image) }}" alt="fullslide1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <div class="tp-caption slider_layer_01 text-center lft tp-resizeme" data-x="center" data-y="200" data-speed="1000" data-start="600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><strong>{{ $slider->title }}</strong>
                    </div>
                    <div class="tp-caption slider_layer_02 text-center lft tp-resizeme" data-x="center" data-y="340" data-speed="1000" data-start="800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">{!! $slider->body !!}
                    </div>
                    <div class="tp-caption text-center lft tp-resizeme" data-x="center" data-y="410" data-speed="1000" data-start="800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><a target="_blank" href="#" class="btn btn-default">{{ $slider->url_name}}</a>
                    </div>
                </li> --}}
                <li data-transition="fade" data-slotamount="1" data-masterspeed="500" data-thumb="#" data-saveperformance="off" data-title="Slide">
                    <img src="{{ asset('storage/'.$slider->image) }}" alt="fullslide1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <div class="tp-caption slider_layer_01 text-center lft tp-resizeme" data-x="center" data-y="220" data-speed="1000" data-start="600" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><i class="fa fa-bookmark-o"></i> {{ $slider->title }}
                    </div>
                    <div class="tp-caption slider_layer_02 text-center lft tp-resizeme" data-x="center" data-y="320" data-speed="1000" data-start="800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;">{!! $slider->body !!}
                    </div>
                    <div class="tp-caption text-center lft tp-resizeme" data-x="center" data-y="390" data-speed="1000" data-start="800" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="1000" style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap;"><a href="{{$slider->url_link}}" class="btn btn-default">{{ $slider->url_name}}</a>
                    </div>
                </li>
            </ul>
            @endforeach
        </div>
    </div>
</section>
@stop

@section('content')
{{-- <section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h4>PROGRAM STUDI TEKNOLOGI REKAYASA PERANGKAT LUNAK - DIPLOMA IV</h4>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<section class="grey page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>PROGRAM STUDI TEKNOLOGI REKAYASA PERANGKAT LUNAK - DIPLOMA IV</h1>
            </div>
        </div>  
    </div>
</section>
<section class="blue section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-left">
                <h4 style="color: white">Kurikulum 2021</h4><hr>
                <p style="color: white; font-family:Arial, Helvetica, sans-serif; font-style:normal">{{config('trpl.kurikulum')}}.
                </p>
                <p style="text-align: center; font-family:Arial, Helvetica, sans-serif; font-style:normal "><a class="btn btn-danger" href="{{ route('course') }}"><i class="fa fa-sign-in"></i> Lihat kurikulum 2021 selengkapnya</a></p>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="why-us text-right wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <i class="fa fa-user alignright"></i>
                    <h4>Dosen Kompeten</h4>
                    <p>Disini kamu akan bertemu dengan dosen-dosen yang kompeten dibidangnya dan juga dosen industri yang telah bekerjasama supaya kamu dapat pengalaman yang luar biasa.</p>
                </div>  
            </div>
        </div> --}}
    </div>
</section>
<section class="section darkskin fullscreen paralbackground parallax" style="background-image:url({{ asset('storage/public/trpl.jpg') }})" data-img-width="1627" data-img-height="868" data-diff="100">
    <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h4>Program Unggulan</h4>
                        <p>Kami memiliki program unggulan prodi</p>
                    </div>
                </div>
            </div>
            <div class="row service-center">
                <div class="col-md-4 col-sm-6">
                    <div class="feature-list">
                        <i class="fa fa-book"></i>
                        <p><strong>IoT bidang Pertanian</strong></p>
                        <p>{{ config('trpl.iot') }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="feature-list">
                        <i class="fa fa-cogs"></i>
                        <p><strong>Machine Learning Pertanian dan Tanaman</strong></p>
                        <p>{{ config('trpl.machinelearning') }}</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="feature-list">
                        <i class="fa fa-camera"></i>
                        <p><strong>Start-Up Hasil Pertanian</strong></p>
                        <p>{{ config('trpl.startup') }}</p>
                    </div>
                </div>
            </div>
        </div>
</section>

<!-- Section untuk berita mulai dari sini -->
<section class="grey section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h4>Seputar Kampus</h4><hr>
                </div>
            </div>
        </div>
        
        <div class="row blog-widget">
            @foreach( $posts as $post )
            <div class="col-md-4 col-sm-6">
                <br>
                <div class="blog-wrapper">
                    <div class="blog-title">
                        <a class="category_title" href="#" title=""></a>
                        <h2><a href="single.html" title="">{{ $post->title }}</a></h2>
                        <div class="post-meta">
                            <span>
                                <i class="fa fa-user"></i>
                                <a href="#">{{ $post->user->name }}</a>
                            </span>
                            <span>
                                <i class="fa fa-tag"></i>
                                @foreach ($categories as $category)  
                                    @if($post->categories->contains($category))    
                                        <a href="#">{{ $category->name }}</a>
                                    @endif
                                @endforeach
                            </span>
                            {{-- <span>
                                <i class="fa fa-comments"></i>
                                <a href="#">19 Comments</a>
                            </span> --}}
                        </div>
                    </div>
                    <div class="blog-image">
                        <a href="{{ route('singlepost', $post->slug) }}" title=""><img src="{{ asset('storage/'.$post->image) }}" class="img-responsive"></a>
                    </div>
                    <div class="blog-desc">
                        <p>{!! (str_word_count($post->body) > 60 ? substr($post->body,0,200).'...' : $post->body) !!}</p>
                        @if (str_word_count($post->body) > 60)
                            <a href="{{ route('singlepost', $post->slug) }}" class="btn btn-info">Baca selengkapnya</a>
                        @endif
                    </div>
                </div>
            </div>    
            @endforeach
        </div>
    </div>
</section>
<!-- End Section untuk berita -->

<!-- Halaman dosen -->
<section class="white section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h4>Tim Dosen TRPL</h4>
                    <p>Kenali lebih dekat</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="content-widget">
                    <div class="team-members row">
                        @foreach($lecturers as $lecturer)
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <br>
                            <div class="team">
                                <img src="{{ asset('storage/'.$lecturer->image) }}" alt="" class="img-responsive wow fadeInUp">
                                <div class="team-hover-content">
                                    <h5>{{ $lecturer->name }}</h5>
                                    <span>{{ $lecturer->position }}</span>
                                    <p>{!! $lecturer->desc !!}</p>
                                    <div class="social">
                                        <a href="{{ $lecturer->fb }}" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        {{-- <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a> --}}
                                        <a href="{{ $lecturer->ig }}" title="Instagram"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End halaman dosen -->

@endsection