@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create new Document</h3>
            </div>

            <form action="{{ route('documents.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Dokumen</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid  @enderror" placeholder="Tuliskan nama dokumen" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="subject">Perihal</label>
                        <input type="text" name="subject" id="subject" class="form-control @error('subject') is-invalid  @enderror" placeholder="Perihal dokumen" value={{ old('subject') }}>
                        @error('subject')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="document">Upload File</label>
                        <input type="file" name="document" id="document" class="form-control @error('document') is-invalid  @enderror" value="{{ old('document') }}">
                        @error('document')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>   

                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category" class="form-control">
                            <option value="akreditasi">Akreditasi</option>
                            <option value="perkuliahan">Dokumen Perkuliahan</option>
                            <option value="pengumuman">Pengumuman</option>
                            <option value="penelitian">Penelitian</option>
                            <option value="pengabdian">Pengabdian Masyarakat</option>
                            <option value="pkl">Praktek Kerja Lapang</option>
                            <option value="skripsi">Skripsi</option>
                        </select>
                    </div>
                    
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
    
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush