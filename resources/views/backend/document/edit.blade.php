@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Document</h3>
            </div>

            <form action="{{ route('documents.update', $document) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Nama Dokumen</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid  @enderror" placeholder="Tuliskan nama dokumen" value="{{ old('name') ?? $document->name }}">
                        @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="subject">Perihal</label>
                        <input type="text" name="subject" id="subject" class="form-control @error('subject') is-invalid  @enderror" placeholder="Perihal dokumen" value="{{ old('subject') ?? $document->subject }}">
                        @error('subject')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="document">Upload File</label>
                        <input type="file" name="document" id="document" class="form-control @error('document') is-invalid  @enderror" value="{{ old('document') }}">
                        @error('document')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>   

                    <div class="form-group">
                        <label for="category">Category</label>
                        <select name="category" class="form-control">
                            <option value="akreditasi" @if ($document->category == "akreditasi") ? selected : '' @endif>Akreditasi</option>
                            <option value="perkuliahan" @if ($document->category == "perkuliahan") ? selected : '' @endif>Dokumen Perkuliahan</option>
                            <option value="pengumuman" @if ($document->category == "pengumuman") ? selected : '' @endif>Pengumuman</option>
                            <option value="penelitian" @if ($document->category == "penelitian") ? selected : '' @endif>Penelitian</option>
                            <option value="pengabdian" @if ($document->category == "pengabdian") ? selected : '' @endif>Pengabdian Masyarakat</option>
                            <option value="pkl" @if ($document->category == "pkl") ? selected : '' @endif>Praktek Kerja Lapang</option>
                            <option value="skripsi" @if ($document->category == "skripsi") ? selected : '' @endif>Skripsi</option>
                        </select>
                    </div>
                    
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
    
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush