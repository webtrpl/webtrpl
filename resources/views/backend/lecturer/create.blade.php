@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Lecturer</h3>
            </div>

            <form action="{{ route('lecturers.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid  @enderror" placeholder="Tuliskan nama" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="position">Position</label>
                        <select name="position" class="form-control">
                            <option value="kaprodi">Ketua Program Studi</option>
                            <option value="mutu">Gugus Kendali Mutu</option>
                            <option value="jurnal">Pengelola Jurnal Ilmiah</option>
                            <option value="kalab">Kepala Laboratorium</option>
                            <option value="dosen">Dosen</option>
                            <option value="plp">PLP</option>
                            <option value="admin">Administrasi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="desc">Description</label>
                        <textarea name="desc" id="summernote" class="form-control @error('desc') is-invalid  @enderror" placeholder="Isi berita">{{ old('desc') }}</textarea>
                        @error('desc')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid  @enderror" value="{{ old('image') }}">
                        @error('image')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>   
                    <div class="form-group">
                        <label for="fb">Facebook</label>
                        <input type="text" name="fb" id="fb" class="form-control @error('fb') is-invalid  @enderror" placeholder="https://www.facebook.com/asep.nurhuda" value="{{ old('fb') }}">
                        @error('fb')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="ig">Instagram</label>
                        <input type="text" name="ig" id="ig" class="form-control @error('ig') is-invalid  @enderror" placeholder="https://www.instagram.com/asepnurhuda/" value="{{ old('ig') }}">
                        @error('ig')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
    
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush