@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Lecturer</h3>
            </div>

            <form action="{{ route('lecturers.update', $lecturer) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid  @enderror" value="{{ old('name') ?? $lecturer->name }}">
                        @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="position">Position</label>
                        <select name="position" class="form-control">
                            <option value="kaprodi" @if ($lecturer->positition == "kaprodi") ? selected : '' @endif>Ketua Program Studi</option>
                            <option value="mutu" @if ($lecturer->positition == "mutu") ? selected : '' @endif>Gugus Kendali Mutu</option>
                            <option value="jurnal" @if ($lecturer->positition == "jurnal") ? selected : '' @endif>Pengelola Jurnal Ilmiah</option>
                            <option value="kalab" @if ($lecturer->positition == "kalab") ? selected : '' @endif>Kepala Laboratorium</option>
                            <option value="dosen" @if ($lecturer->positition == "dosen") ? selected : '' @endif>Dosen</option>
                            <option value="plp" @if ($lecturer->positition == "plp") ? selected : '' @endif>PLP</option>
                            <option value="admin" @if ($lecturer->positition == "admin") ? selected : '' @endif>Administrasi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="desc">Description</label>
                        <textarea name="desc" id="summernote" class="form-control @error('desc') is-invalid  @enderror">{{ old('desc') ?? $lecturer->desc }}</textarea>
                        @error('desc')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid  @enderror" value="{{ old('image') }}">
                        @error('image')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>   
                    <div class="form-group">
                        <label for="fb">Facebook</label>
                        <input type="text" name="fb" id="fb" class="form-control @error('fb') is-invalid  @enderror" placeholder="https://www.facebook.com/asep.nurhuda" value="{{ old('fb') ?? $lecturer->fb }}">
                        @error('fb')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="ig">Instagram</label>
                        <input type="text" name="ig" id="ig" class="form-control @error('ig') is-invalid  @enderror" placeholder="https://www.instagram.com/asepnurhuda/" value="{{ old('ig') ?? $lecturer->ig }}">
                        @error('ig')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
    
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush