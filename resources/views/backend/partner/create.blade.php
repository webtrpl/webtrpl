@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Partner</h3>
            </div>

            <form action="{{ route('partners.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Owner Name</label>
                        <input type="name" name="name" id="name" class="form-control @error('name') is-invalid  @enderror" placeholder="Tuliskan nama pemilik perusahaan atau pimpinan institusi" value="{{ old('name') }}">
                        @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">Company Logo</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid  @enderror" value="{{ old('image') }}">
                        @error('image')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="company">Company Name</label>
                        <input type="company" name="company" id="company" class="form-control @error('company') is-invalid  @enderror" placeholder="Tuliskan nama perusahaan atau institusi" value="{{ old('company') }}">
                        @error('company')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="city" name="city" id="city" class="form-control @error('city') is-invalid  @enderror" placeholder="Kota asal" value="{{ old('city') }}">
                        @error('city')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="field">Field</label>
                        <input type="field" name="field" id="field" class="form-control @error('field') is-invalid  @enderror" placeholder="Bidang perkerjaan" value="{{ old('field') }}">
                        @error('field')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid  @enderror" placeholder="Email Perusahaan atau instansi" value="{{ old('email') }}">
                        @error('email')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="website">Website</label>
                        <input type="website" name="website" id="website" class="form-control @error('website') is-invalid  @enderror" placeholder="Website Perusahaan atau instansi" value="{{ old('website') }}">
                        @error('website')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>  
                    <div class="form-group">
                        <label for="photo">Owner Photo</label>
                        <input type="file" name="photo" id="photo" class="form-control @error('photo') is-invalid  @enderror" value="{{ old('photo') }}">
                        @error('photo')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="comment">Comment</label>
                        <textarea name="comment" id="summernote" class="form-control @error('comment') is-invalid  @enderror">{{ old('comment') }}</textarea>
                        @error('comment')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>    
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
    
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })

            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endpush