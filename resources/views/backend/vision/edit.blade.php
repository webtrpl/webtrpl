@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Edit Vision</h3>
            </div>

            <form action="{{ route('visions.update', $vision) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="welcome">Welcome Speech Head Of TRPL</label>
                        <textarea name="welcome" id="welcome" class="form-control summernote @error('welcome') is-invalid  @enderror"> {{ old('welcome') ?? $vision->welcome }} </textarea>
                        @error('welcome')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="vision">Our Vision</label>
                        <textarea name="vision" id="vision" class="form-control summernote @error('vision') is-invalid  @enderror"> {{ old('vision') ?? $vision->vision}} </textarea>
                        @error('vision')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="mission">Missions</label>
                        <textarea name="mission" id="mission" class="form-control summernote @error('mission') is-invalid  @enderror"> {{ old('mission') ?? $vision->mission }} </textarea>
                        @error('mission')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="goal">Goals</label>
                        <textarea name="goal" id="goal" class="form-control summernote @error('goal') is-invalid  @enderror"> {{ old('goal') ?? $vision->goal }} </textarea>
                        @error('goal')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="target">Targets</label>
                        <textarea name="target" id="target" class="form-control summernote @error('target') is-invalid  @enderror"> {{ old('target') ?? $vision->target }} </textarea>
                        @error('target')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image" class="form-control @error('image') is-invalid  @enderror">
                        @error('image')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>

        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            // Summernote
            $('.summernote').summernote()
        })
    </script>
@endpush