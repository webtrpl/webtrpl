@extends('backend.master')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Edit Course</h3>
            </div>

            <form action="{{ route('courses.update', $course) }}" method="post">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="code">Course Code</label>
                        <input type="text" name="code" id="code" class="form-control @error('code') is-invalid  @enderror" placeholder="Enter a course code" value="{{ old('code') ?? $course->code }}">
                        @error('code')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">Course Name</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid  @enderror" placeholder="Enter a course name" value="{{ old('name') ?? $course->name }}">
                        @error('name')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="credit">Credit</label>
                        <input type="text" name="credit" id="credit" class="form-control @error('credit') is-invalid  @enderror" placeholder="Enter number of credit" value="{{ old('credit') ?? $course->credit}}">
                        @error('credit')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="semester">Semester</label>
                        <input type="text" name="semester" id="semester" class="form-control @error('semester') is-invalid  @enderror" placeholder="Enter number of semester" value="{{ old('semester') ?? $course->semester}}">
                        @error('semester')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control @error('description') is-invalid  @enderror"> {{ old('description') ?? $course->description }} </textarea>
                        @error('description')
                            <span class="invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>

        </div>
    </div>
</div>

@stop

@push('style')
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.min.css') }}">
@endpush

@push('script')
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function(){
            // Summernote
            $('#description').summernote()
        })
    </script>
@endpush