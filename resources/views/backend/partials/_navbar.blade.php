    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        <!-- Sign Out Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">{{ auth()->user()->name }}
            <i class="fas fa-user-circle"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                <a href="#" class="nav-link" onclick="document.getElementById('logout').submit()">
                <i class="fas fa-sign-out-alt"></i> Log Out
                </a>
                <form id="logout" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                    <input type="hidden">
                </form>
            </div>
        </li>
        
        </ul>
    </nav>