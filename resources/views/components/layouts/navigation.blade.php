
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
        <img src="{{ asset('storage/logotrpl.png') }}" alt="TRPL Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Prodi TRPL</span>
        </a>
    
        <!-- Sidebar -->
        <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
            <img src="{{ asset('storage/default.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
            <a href="#" class="d-block">{{ auth()->user()->name}}</a>
            </div>
        </div>
    
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Dashboard
                    </p>
                    </a>
                </li>
                @hasanyrole($roles)
                @can('show visions')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-trophy"></i>
                    <p>
                        Our Profiles
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('visions.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Vision and Mission</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('governances.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Governance</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('symbols.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Symbol</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('show permissions')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-cogs"></i>
                    <p>
                        Role and Permissions
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('roles.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Roles</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('permissions.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Permissions</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('assign.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Give Permissions</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('show categories')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tags"></i>
                    <p>
                        Categories
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('categories.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Categories</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan


                @can('show posts')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-pencil-alt"></i>
                    <p>
                        Posts
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('posts.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Posts</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('posts.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Create Posts</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('show courses')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        Courses
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('courses.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Courses</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('courses.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Create Course</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('show sliders')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-sliders-h"></i>
                    <p>
                        Sliders
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('sliders.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Sliders</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('sliders.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Create Slider</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('show lectures')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-users"></i>
                    <p>
                        Lecturers
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('lecturers.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Lecturers</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('lecturers.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Create Lecturer</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan

                @can('show partners')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-handshake"></i>
                    <p>
                        Partners
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('partners.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Partners</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('partners.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add Partner</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('show documents')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-file"></i>
                    <p>
                        Documents
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('documents.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Documents</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('documents.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add Documents</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('show comments')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-comment"></i>
                    <p>
                        Comments
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('comments.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Comments</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('show facilities')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-plane"></i>
                    <p>
                        Facilities
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('facilities.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Facilities</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('facilities.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add Facility</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('show himas')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-child"></i>
                    <p>
                        Himpunan Mahasiswa
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('himas.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Hima</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('himas.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add Hima</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @can('show users')
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        Users
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('users.index') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Show Users</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('users.create') }}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add User</p>
                            </a>
                        </li>
                    </ul>
                </li>
                @endcan
                @endhasanyrole
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>
