<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWelcomeToVisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visions', function (Blueprint $table) {
            $table->string('welcome')->after('id');
            $table->string('image')->after('target');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visions', function (Blueprint $table) {
            $table->dropColumn('welcome');
            $table->dropColumn('image');
        });
    }
}
