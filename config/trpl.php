<?php

return [
    'iot' => 'Internet of Thing (IoT) adalah sebuah konsep dimana suatu objek yang memiliki kemampuan untuk mentransfer data melalui jaringan tanpa memerlukan interaksi manusia ke manusia atau manusia ke komputer. IoT telah berkembang dari konvergensi teknologi nirkabel, micro-electromechanical systems (MEMS), dan Internet',
    'machinelearning' => 'Teknologi machine learning (ML) adalah mesin yang dikembangkan untuk bisa belajar dengan sendirinya tanpa arahan dari penggunanya. Pembelajaran mesin dikembangkan berdasarkan disiplin ilmu lainnya seperti statistika, matematika dan data mining sehingga mesin dapat belajar dengan menganalisa data tanpa perlu di program ulang atau diperintah.',
    'startup' => 'Startup adalah sebuah usaha yang baru berjalan dan menerapkan inovasi teknologi untuk menjalankan core business-nya & memecahkan sebuah masalah di masyarakat. Saat ini prodi TRPL sedang mengembangkan start-up untuk pertanian khususnya yang berkaitan dengan prodi yang ada di Politeknik Pertanian Negeri Samarinda',
    'kurikulum' => 'Mulai Tahun Akademik 2021/2022, Program Studi Teknologi Rekayasa Perangkat Lunak menjalankan kurikulum terbaru, yakni Kurikulum 2021. Kurikulum ini mendukung kebijakan Merdeka Belajar Kampus Merdeka. Mahasiswa dapat menempuh sejumlah mata kuliah di luar program studi, baik di lingkungan Politani Samarinda atau bahkan di Perguruan Tinggi lain. Kurikulum ini merupakan penyempurnaan dari kurikulum kami sebelumnya, yakni Kurikulum 2016'
];
